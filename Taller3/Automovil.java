package taller3;

import java.util.Scanner;

public class Automovil {
	String tamaño;
    String marca;
    int año;

    public Automovil(String tamaño, String marca, int año) {
        this.tamaño = tamaño;
        this.marca = marca;
        this.año = año;
    }

    public void determinarCaracteristicas() {
        int velocidad;
        int fuerza;

        switch (tamaño) {
            case "pequeño":
                velocidad = 150;
                fuerza = 100;
                break;
            case "mediano":
                velocidad = 120;
                fuerza = 150;
                break;
            case "grande":
                velocidad = 100;
                fuerza = 200;
                break;
            default:
                System.out.println("Tamaño no reconocido. Usando valores por defecto.");
                velocidad = 120;
                fuerza = 100;
                break;
        }

        System.out.println("Características del automóvil:");
        System.out.println("Tamaño: " + tamaño);
        System.out.println("Marca: " + marca);
        System.out.println("Año: " + año);
        System.out.println("Velocidad: " + velocidad + " km/h");
        System.out.println("Fuerza: " + fuerza + " CV");
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("¿Qué tamaño de automóvil quieres (pequeño/mediano/grande)?: ");
        String tamaño = scanner.nextLine();

        System.out.print("¿Qué marca de automóvil prefieres?: ");
        String marca = scanner.nextLine();

        System.out.print("¿De qué año es el automóvil?: ");
        int año = scanner.nextInt();

        Automovil automovil = new Automovil(tamaño, marca, año);

        System.out.println("\nBuscando automóvil con esas características...");
        System.out.println("¡Sí, tenemos un auto con esas características!");

        automovil.determinarCaracteristicas();
    }
}
