package taller3;

public class Receta {
    String[] ingredientes;
    int tiempoPreparacion;

    public Receta(String[] ingredientes, int tiempoPreparacion) {
        this.ingredientes = ingredientes;
        this.tiempoPreparacion = tiempoPreparacion;
    }

    public void imprimirIngredientes() {
        System.out.println("Ingredientes para el arroz con pollo:");
        for (String ingrediente : ingredientes) {
            System.out.println("- " + ingrediente);
        }
    }

    public void imprimirTiempoPreparacion() {
        System.out.println("Tiempo de preparación: " + tiempoPreparacion + " minutos");
    }

    public static void main(String[] args) {
        String[] ingredientesArrozConPollo = {
                "Arroz",
                "Pollo",
                "Zanahoria",
                "Pimiento",
                "Cebolla",
                "Ajo",
                "Aceite",
                "Sal",
                "Pimienta",
                "Caldo de pollo"
        };

        int tiempoPreparacionArrozConPollo = 45;

        Receta recetaArrozConPollo = new Receta(ingredientesArrozConPollo, tiempoPreparacionArrozConPollo);

        recetaArrozConPollo.imprimirIngredientes();
        recetaArrozConPollo.imprimirTiempoPreparacion();

        System.out.println("¡El arroz con pollo está listo! ¡La comida está servida!");
    }
}
