package taller3;

import java.util.Scanner;

public class Perro {
	 String raza;
	    String tamaño;
	    String tipoPelaje;
	    String colorPelaje;

	    public void preguntarSobreAdopcion() {
	        Scanner scanner = new Scanner(System.in);

	        System.out.print("¿Quieres adoptar un perro? (si/no): ");
	        String respuesta = scanner.nextLine();

	        if (respuesta.equalsIgnoreCase("si")) {
	            System.out.print("¿Qué raza estás buscando?: ");
	            raza = scanner.nextLine();

	            System.out.print("¿Qué tamaño de perro prefieres?: ");
	            tamaño = scanner.nextLine();

	            System.out.print("¿Qué tipo de pelaje te gustaría?: ");
	            tipoPelaje = scanner.nextLine();

	            System.out.print("¿De qué color debería ser el pelaje?: ");
	            colorPelaje = scanner.nextLine();

	            System.out.println("\nEstas son las características del perro que estás buscando:");
	            mostrarCaracteristicas();

	            System.out.println("\n¡Enhorabuena si tenemos un perro con esas características!");
	        } else if (respuesta.equalsIgnoreCase("no")) {
	            System.out.println("Entiendo, ¡muchas gracias por considerar la adopción de un perro!");
	        } else {
	            System.out.println("Respuesta no válida. Por favor, responde 'Sí' o 'No'.");
	        }
	    }

	    public void mostrarCaracteristicas() {
	        System.out.println("Raza: " + raza);
	        System.out.println("Tamaño: " + tamaño);
	        System.out.println("Tipo de Pelaje: " + tipoPelaje);
	        System.out.println("Color de Pelaje: " + colorPelaje);
	    }

	    public static void main(String[] args) {
	        Perro perro = new Perro();
	        perro.preguntarSobreAdopcion();
	    }
}
