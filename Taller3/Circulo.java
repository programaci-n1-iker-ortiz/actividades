package taller3;

import java.util.Scanner;

public class Circulo {
	 double radio;

	    public Circulo(double radio) {
	        this.radio = radio;
	    }

	    public double calcularArea() {
	        return Math.PI * Math.pow(radio, 2);
	    }

	    public double calcularPerimetro() {
	        return 2 * Math.PI * radio;
	    }

	    public static void main(String[] args) {
	        Scanner scanner = new Scanner(System.in);

	        System.out.print("Ingresa el radio del círculo: ");
	        double radio = scanner.nextDouble();

	        Circulo circulo = new Circulo(radio);

	        double area = circulo.calcularArea();
	        double perimetro = circulo.calcularPerimetro();

	        System.out.println("El área del círculo es: " + area);
	        System.out.println("El perímetro del círculo es: " + perimetro);
	    }
}
