package taller3;

public class Publicidad {
	 String titulo;
	    String contenido;
	    String duracionOferta;

	    public Publicidad(String titulo, String contenido, String duracionOferta) {
	        this.titulo = titulo;
	        this.contenido = contenido;
	        this.duracionOferta = duracionOferta;
	    }

	    public void mostrarPublicidad() {
	        System.out.println("---- Anuncio Publicitario ----");
	        System.out.println("Título: " + titulo);
	        System.out.println("Contenido: " + contenido);
	        System.out.println("Duración de la oferta: " + duracionOferta);
	        System.out.println("-------------------------------");
	    }

	    public static void main(String[] args) {
	        Publicidad anuncio = new Publicidad("Oferta Especial", "¡Compra ahora y obtén un 20% de descuento!", "Hasta el 30 de septiembre");

	        anuncio.mostrarPublicidad();
	    }
}
