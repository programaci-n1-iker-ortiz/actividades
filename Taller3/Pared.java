package taller3;

import java.util.Scanner;

public class Pared {
	 String diseño;
	    String color;

	    public Pared(String diseño, String color) {
	        this.diseño = diseño;
	        this.color = color;
	    }

	    public void pintarConAerosol() {
	        System.out.println("Pintando la pared con aerosol. Diseño: " + diseño + ", Color: " + color);
	    }

	    public void pintarConBrocha() {
	        System.out.println("Pintando la pared con pintura a brocha. Diseño: " + diseño + ", Color: " + color);
	    }

	    public void pintarConRodillo() {
	        System.out.println("Pintando la pared con rodillo. Diseño: " + diseño + ", Color: " + color);
	    }

	    public void pintarConPincel() {
	        System.out.println("Pintando la pared con pincel. Diseño: " + diseño + ", Color: " + color);
	    }

	    public static void main(String[] args) {
	        Scanner scanner = new Scanner(System.in);

	        System.out.print("¿Qué diseño quieres para la pared? ");
	        String diseño = scanner.nextLine();

	        System.out.print("¿De qué color quieres pintar la pared? ");
	        String color = scanner.nextLine();

	        Pared pared = new Pared(diseño, color);

	        System.out.println("Elige el método de pintura:");
	        System.out.println("1. Aerosol");
	        System.out.println("2. Pintura a brocha");
	        System.out.println("3. Rodillo");
	        System.out.println("4. Pincel");

	        int metodo = scanner.nextInt();

	        switch (metodo) {
	            case 1:
	                pared.pintarConAerosol();
	                break;
	            case 2:
	                pared.pintarConBrocha();
	                break;
	            case 3:
	                pared.pintarConRodillo();
	                break;
	            case 4:
	                pared.pintarConPincel();
	                break;
	            default:
	                System.out.println("Método no válido.");
	        }
	    }
}
