package taller3;

public class Galleta {
	 String tipo;
	    double precio;
	    String sabor;
	    String color;

	    public Galleta(String tipo, double precio, String sabor, String color) {
	        this.tipo = tipo;
	        this.precio = precio;
	        this.sabor = sabor;
	        this.color = color;
	    }

	    public void imprimirInfo() {
	        System.out.println("Tipo: " + tipo);
	        System.out.println("Precio: $" + precio);
	        System.out.println("Sabor: " + sabor);
	        System.out.println("Color: " + color);
	    }

	    public static void main(String[] args) {
	        Galleta galleta1 = new Galleta("Chocolate", 2.5, "Chocolate", "Marrón");

	        galleta1.imprimirInfo();

	        System.out.println("El pedido de galletas ha sido empacado.");
	    }
}
