package taller3;

import java.util.Scanner;

public class Humano {
	 String nombre;
	    int edad;
	    String genero;

	    public void obtenerInformacion() {
	        Scanner scanner = new Scanner(System.in);

	        System.out.print("¿Cuál es tu nombre?: ");
	        nombre = scanner.nextLine();

	        System.out.print("¿Cuál es tu edad?: ");
	        edad = scanner.nextInt();
	        scanner.nextLine(); 

	        System.out.print("¿Cuál es tu género?: ");
	        genero = scanner.nextLine();
	    }

	    public void mostrarInformacion() {
	        System.out.println("Información del humano:");
	        System.out.println("Nombre: " + nombre);
	        System.out.println("Edad: " + edad + " años");
	        System.out.println("Género: " + genero);
	    }

	    public static void main(String[] args) {
	        Humano humano = new Humano();
	        humano.obtenerInformacion();
	        humano.mostrarInformacion();
	    }
}
