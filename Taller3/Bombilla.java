package taller3;

import java.util.Scanner;

public class Bombilla {
	 String tamaño;
	    String color;
	    String forma;
	    int intensidad;

	    public Bombilla(String tamaño, String color, String forma, int intensidad) {
	        this.tamaño = tamaño;
	        this.color = color;
	        this.forma = forma;
	        this.intensidad = intensidad;
	    }

	    public void imprimirInfo() {
	        System.out.println("Tamaño: " + tamaño);
	        System.out.println("Color: " + color);
	        System.out.println("Forma: " + forma);
	        System.out.println("Intensidad: " + intensidad + "W");
	    }

	    public static void main(String[] args) {
	        Scanner scanner = new Scanner(System.in);

	        System.out.print("¿Qué tamaño quieres para la bombilla? ");
	        String tamaño = scanner.nextLine();

	        System.out.print("¿De qué color quieres la bombilla? ");
	        String color = scanner.nextLine();

	        System.out.print("¿Qué forma prefieres para la bombilla? ");
	        String forma = scanner.nextLine();

	        System.out.print("¿Qué intensidad en Watts desea la bombilla? ");
	        int intensidad = scanner.nextInt();

	        Bombilla bombilla = new Bombilla(tamaño, color, forma, intensidad);

	        System.out.println("Características de la bombilla:");
	        bombilla.imprimirInfo();
	    }
}
